# UA Bootstrap
UA's flavor of the Bootstrap Framework, combining the new UA Brand with the
goodness of Bootstrap 3.


## Using UA Bootstrap

### CDN
Instructions for using the UA Digital CDN to include UA Bootstrap in your project
can be found on the homepage:

[http://uadigital.arizona.edu/ua-bootstrap/](http://uadigital.arizona.edu/ua-bootstrap/)

### NPM
Add the latest UA Bootstrap version to a project with NPM using the command
```
npm install --save https://bitbucket.org/uadigital/ua-bootstrap.git
```
or by adding the following line to the dependencies section in your `package.json` file
```
"ua-bootstrap": "https://bitbucket.org/uadigital/ua-bootstrap.git"
```
Additional information on specifying a version of the dependency in NPM can be
found in the NPM API's [install section](https://docs.npmjs.com/cli/install)

### Versioning scheme
UA Bootstrap uses the [SemVer](http://semver.org/) scheme to provide informative
version numbers for each release in the form of MAJOR.MINOR.PATCH.

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards-compatible manner, and
3. PATCH version when you make backwards-compatible bug fixes.


## Contributing

### Dependencies
* [Git](https://git-scm.com)
* [NPM](https://www.npmjs.com/)
* [Gulp](http://gulpjs.com/)

### Setup for development
(Items shown `like this` are commands to be run in your terminal.)

1. Install [NodeJS](https://nodejs.org/en/download/) & [Git](https://git-scm.com/downloads)
2. Create [BitBucket](https://bitbucket.org) account
3. Clone the repository: `git clone git@bitbucket.org:uadigital/ua-bootstrap.git`
4. Navigate to the repository's directory: `cd ua-bootstrap`
5. Install gulp: `npm install gulp -g`
6. Install dependencies (and build): `npm install`
7. Start development server: `gulp serve`
8. Open [http://localhost:8888](http://localhost:8888) in your browser, and voilà.

### Testing
With the preview server running (`gulp serve`), run the following in a separate
shell session.
```
npm run backstop_reference
npm run backstop_test
```
To open the results page in your browser:
```
npm run backstop_openReport
```

### On making edits to UA Bootstrap
UA Bootstrap is entirely based on the Bootstrap Framework and thus is largely
a bunch of overrides. Sometimes Bootstrap doesn't have eveything we'd like in
UA Boostrap, so when adding a feature that is not an override to Vanilla
Bootstrap, place those additions/appendages in `_custom.scss`.


## Development tips

### Making edits in Chrome persistent
Picking up from the last step, you should now have the Bootstrap documentation running
locally. Now to wire up Chrome DevTools to write changes out to the files you
are serving up. Go back to the browser and fire up Chrome DevTools and head over
to the __Sources__ tab. There should be a __Sources__ pane with a list of
domains where all your assets are being downloaded from. Right click in that pane
and select __Add Folder to Workspace__.

![Add Folder to
Workspace](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-1.png)

Add the directory where the ua-bootstrap repo is located. For me it was on my
Desktop so I added `~/Desktop/ua-bootstrap`. Now the folder is added to your
workspace. Now to link the files to the folder in your workspace. Go back to the
__Sources__ pane and right-click `ua-bootstrap.scss` in the folder titled
`source` under `localhost:8888`. Click __Map to File System Resource...__ and it
should auto-find the file in the added workspace folder. Hit enter and you're
set.

![Map to File System Resource...](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-2.png)

![ua-bootstrap.scss](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-3.png)

One caveat on using Chrome inspector to edit. Chrome does not have the ability
to make edits to the `*.scss` under the __Elements__ tab. You'll have to make
all edits in the __Sources__ tab, which is a bit of a pain. So find the files
you need to override in the __Elements__ tab, but then select the reference link
to bring you to the __Sources__ tab where you can make the edit and save.

![Make edit in Sources Tab](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-4.png)
