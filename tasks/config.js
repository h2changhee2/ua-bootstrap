const buildDir = './build'

module.exports = {
  bs: require('browser-sync').create(),
  bsConfig: {
    server: {
      baseDir: buildDir
    },
    port: 8888,
    notify: false
  },

  buildDir,
  distDir: './dist',
  docsDir: './docs',
  srcDir: './src',

  bootstrapDir: './node_modules/bootstrap-sass/assets',
  glyphiconsRemove: '@import "bootstrap/glyphicons";'
}
